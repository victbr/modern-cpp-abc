#include <string>
#include <iostream>
#include <concepts>
#include <cstddef>

using namespace std;

template <typename T>
constexpr bool is_std_string_like =
    // is_string<T>::value || is_convertible<T, std_string_view<char>>::value ||
    requires(T* p) {
        p->find('a');
        p->length();
        p->data();
    };

int main(){
    auto ret = is_std_string_like<std::string>;
    cout << ret << endl;
    ret = is_std_string_like<int>;
    cout << ret << endl;
    return 0;
}