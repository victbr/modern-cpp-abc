#include <cstddef>
#include <iostream>
#include <string>
#include <type_traits>

using namespace std;

template <typename T> class is_std_string_like {
private:
    using yes_type = char;
    struct no_type {
        char x[2];
    };
    template <typename U>
    static auto check(U *p) -> decltype((void)p->find('a'), p->length(),
                                        (void)p->data(), yes_type());
    template <typename> static no_type check(...);

public:
    // static const bool value = is_string<T>::value ||
    //                           is_convertible<T, std_string_view<char>>::value
    //                           || sizeof(check<T>(0)) == sizeof(yes_type);
    static constexpr bool value = is_same<decltype(check<T>(nullptr)),
                                          yes_type>::value;
};

int main() {
    auto ret = is_std_string_like<std::string>::value;
    cout << ret << endl;
    ret = is_std_string_like<int>::value;
    cout << ret << endl;
    return 0;
}