#include <string>
// #include <cstddef>
#include <iostream>
#include <utility>

using namespace std;

// template <typename T> T declval();
template <typename T> class is_std_string_like {
private:
    typedef char yes_type;
    struct no_type {
        char x[2];
    };
    template <typename U>
    static yes_type check(char (
            *)[sizeof((void)declval<U *>()->find('a'), declval<U *>()->length(),
                      (void)declval<U *>()->data(), int())]);
    template <typename> static no_type check(...);

public:
    // static const bool value = is_string<T>::value ||
    //                           is_convertible<T, std_string_view<char>>::value ||
    //                           sizeof(check<T>(0)) == sizeof(yes_type);
    static const bool value = sizeof(check<T>(nullptr)) == sizeof(yes_type);
};

int main() {
    auto ret = is_std_string_like<std::string>::value;
    cout << ret << endl;
    ret = is_std_string_like<int>::value;
    cout << ret << endl;
    return 0;
}