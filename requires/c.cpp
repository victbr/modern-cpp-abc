#include <cstddef>
#include <iostream>
#include <string>
#include <type_traits>

using namespace std;

template<typename, typename=void>
constexpr bool check = false;
template<typename U>
constexpr bool check<U, void_t<decltype(declval<U*>()->find('a')),
                                decltype(declval<U*>()->length()),
                                decltype(declval<U*>()->data())>> = true;

template <typename T> constexpr bool is_std_string_like = check<T>;

int main() {
    auto ret = is_std_string_like<std::string>;
    cout << ret << endl;
    ret = is_std_string_like<int>;
    cout << ret << endl;
    return 0;
}