#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

void fold_expression() {
  auto sum = [](auto... args) { return (args + ...); };
  cout << sum(1, 2, 3, 4, 5) << endl;
  cout << sum(1.1, 2.2, 3.3, 4.4, 5.5) << endl;
  cout << sum(string("a"), string("b"), string("c")) << endl;
  // cout << sum("a", "b", "c") << endl; error
  // 这里应该是有歧义？会首先转换成const char*而不是string
}

void constexpr_lambda() {
  constexpr auto add = [](int a, int b) { return a + b; };
  static_assert(add(1, 2) == 3, "error");
  cout << add(1, 2) << endl;
}

/**
 * @brief 通过*this捕获成员变量的拷贝而不是引用，避免生命周期相关的问题。
 * 但是使用场景存疑？
 *
 */
void capture_this() {
  struct A {
    int a;
    void func() {
      cout << "a: " << a << endl;
      auto g = [this]() {
        a += 1;
        return a;
      };
      cout << "g(): " << g() << endl;
      auto f = [*this]() {
        // a += 1; //error, assignment of member ‘capture_this()::A::a’ in
        // read-only object
        return a;
      }; /// 这里捕获的是拷贝。不会出现生命周期相关的问题。但是这里捕获的值又不能修改，是一个奇怪的右值？
      cout << "f(): " << f() << endl;
      cout << "a: " << a << endl;
    }
  };
  A a{1};
  a.func();
};

int main() {
  cout << "fold expresssion: \n";
  fold_expression();
  cout << "constexpr lambda: \n";
  constexpr_lambda();
  cout << "capture this: \n";
  capture_this();
}