#include <iostream>
#include <string>
#include <tuple>
#include <vector>

using namespace std;

/**
 * @brief STL模板的构造函数的类型自动推导
 *
 */
void STL_deduction() {
  pair<int, double> p(1, 2.2); // before c++17
  cout << p.first << " " << p.second << endl;

  pair p2(1, 2.2); // since c++17
  cout << p2.first << " " << p2.second << endl;
  vector v = {1, 2, 3}; // since c++17
  for (auto i : v) {
    cout << i << " ";
  }
  cout << endl;
}

/**
 * @brief 结构化绑定
 *
 */
void structure_bind() {
  auto func = []() -> tuple<int, double> { return tuple(1, 2.2); };
  auto [a, b] = func();
  cout << a << " " << b << endl;

  pair p(1, 2.2);
  auto &[c, d] = p;
  cout << p.first << " " << p.second << endl; // 1 2.2
  c = 2;
  cout << p.first << " " << p.second << endl; // 2 2.2

  // constexpr auto [x, y] = pair(1, 2.2f); // since c++20? g++9.4 不行

  int array[3] = {1, 2, 3};
  auto [x, y, z] = array;
  cout << x << " " << y << " " << z << endl;

  struct S {
    int a;
    double b;
  };
  S s{1, 2.2};
  auto [m, n] = s;
  cout << m << " " << n << endl;
}

class Entry {
public:
  void Init() {
    name_ = "name";
    age_ = 20;
  }
  string GetnName() const { return name_; }
  int GetAge() const { return age_; }

private:
  std::string name_;
  int age_;
};

template <size_t I> auto get(const Entry &e) {
  if constexpr (I == 0) {
    return e.GetnName();
  } else if constexpr (I == 1) {
    return e.GetAge();
  }
}

namespace std {
template <> struct tuple_size<Entry> : integral_constant<size_t, 2> {};
template <size_t I> struct tuple_element<I, Entry> {
  using type = decltype(get<I>(declval<Entry>())); /// declval的作用是在编译期生成一个临时的为右边类型的对象，用于类型推导， 也可以用来检查是否具有某个成员函数
}; /// copilot 自动补全的代码
// template <> struct tuple_element<0, Entry> { using type = string; };
// template <> struct tuple_element<1, Entry> { using type = int; }; //
// 也可以这样写

} // namespace std

/**
 * @brief 自定义结构化绑定，
 * 缺点在于需要在std里添加东西，不然会默认绑成员变量，而此时成员变量是私有的
 *
 * @todo
 * 原理还没有搞清楚，但是应该无法避免修改std，因为对stl的结构化绑定应该是通过std里的什么东西实现的？
 * 结构化绑定应该是通过某种转换成tuple的方式实现的，而tuple又是实现在std里面的。
 */
void bind_custom() {
  Entry e;
  e.Init();
  auto [name, age] = e;
  cout << name << " " << age << endl;
}

void if_switch() {
  auto GetValue = []() -> int { return 1; };
  if (auto value = GetValue(); value > 0) {
    cout << "value > 0" << endl;
  } else {
    cout << "value <= 0" << endl;
  }
}


void other_test(){
    auto a = integral_constant<int, 1>();
    cout << "a.value: "<<a.value << ", a():" << a()   << endl;
    struct A{
        using type = int;
    };
    cout << typeid(A::type).name() << endl; /// 看起来是相关组件使用的时候默认了tuple_element有一个type成员类型
    /// 看起来，在tuple的使用中，会默认使用tuple_element的type成员类型，然后使用输入的int作为index，tuple_size是用来确定边界的？
}
int main() {
  cout << "STL_deduction: \n";
  STL_deduction();
  cout << "structure_bind: \n";
  structure_bind();
  bind_custom();
  cout << "initiate if-switch: \n";
  if_switch();
  /// 内联变量是c++17开始才支持的
    other_test();
  return 0;
}