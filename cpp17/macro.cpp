#include <iostream>
#include <optional>
#include <string>

#if defined __has_include
#if __has_include(                                                             \
    <format>) /// 用于判断当前的g++环境是否有某个库， 而不是判断当前是否include了这个库
#define HAS_STRING 1
#endif
#endif

using namespace std;

optional<int> has_string() {
#ifdef HAS_STRING
  return 1;
#else
  return nullopt;
#endif
}

void has_include() {
  auto res = has_string();
  if (res) {
    cout << "has string" << endl;
  } else {
    cout << "no string" << endl;
  }
}

[[deprecated("use new_func instead")]] void old_func() {
  cout << "old func" << endl;
}

[[nodiscard]] int new_func() {
  cout << "new func" << endl;
  return 1;
}

[[maybe_unused]] void unused_func() {
  cout << "unused func" << endl;
  int x = 1;
}

void fallthrough(int x) {
  switch (x) {
  case 1:
    cout << "1" << endl;
    [[fallthrough]];
  case 2:
    cout << "2" << endl;
    break;
  case 3:
    cout << "3" << endl;
  default:
    cout << "default" << endl;
  }
}

int main() {
  has_include();
  // old_func(); //warning, is deprecated
  [[maybe_unused]] auto ret = new_func(); // warning, result is not used
  unused_func();
  cout << "fallthrough: " << endl;
  fallthrough(1);
    fallthrough(3);
}
