#include <iostream>
#include <string>
#include <tuple>
#include <variant>
#include <optional>
#include <any>
#include <charconv>

using namespace std;

/**
 * @brief variant的使用， variant的获取还是只能通过index？然后不同的类型会返回不同的index值，然后通过index值获取variant的值。
 * 
 */
void variant_test(){
    std::variant<int, string> var("hello");
    cout << var.index() << endl; /// 这里居然只是获取index？？
    var = 123;
    cout << var.index() << endl;   

    auto str = std::get<0>(var);
    cout << str << endl;
    // auto i = std::get<1>(var); /// 这里会抛出异常
    var = "world";
    cout << std::get<string>(var) << endl;

    struct A{
        int a;
        A(int a) : a(a){}
    };
    std::variant<A, int> var2(1); ///第一个类型必须要有默认构造函数
    cout << var2.index() << endl; /// 这里输出1，说明自动匹配了类型int
    std::variant<A, int> var3(A(1));
    cout << var3.index() << endl; /// 这里输出0，说明匹配了类型A

    std::variant<std::monostate, A, string> var4; /// 如果都没有默认构造函数，则可以用monostate做第一个（打桩？那其实这里用个int是不是也行？）
    cout << var4.index() << endl; /// 这里输出0，说明匹配了类型monostate
}

void optional_test(){
    std::optional<int> opt;
    cout << opt.has_value() << endl; /// 这里输出0
    opt = 1;
    cout << opt.has_value() << endl; /// 这里输出1
    cout << *opt << endl; /// 这里输出1
    cout << opt.value() << endl; /// 这里输出1
    opt.reset();
    cout << opt.has_value() << endl; /// 这里输出0
}

void any_test(){
    std::any a;
    a = 1;
    cout << a.type().name() << " " <<  std::any_cast<int>(a) << endl;
    a = string("hello"); ///这里的问题还是不能默认识别string
    cout << a.type().name() << " " <<  std::any_cast<string>(a) << endl;
}

/**
 * @brief apply 类似于python中的*args, **kwargs，可以将tuple中的元素展开作为参数传递给函数
 * *args是list， **kwarfs是dict
 * 
 */
void apply_test(){
    auto t = std::make_tuple(1, 2, 3);
    auto f = [](int a, int b, int c){
        cout << a << " " << b << " " << c << endl;
    };
    std::apply(f, t);
}

/**
 * @brief 作为构造函数的替代，可以将tuple中的元素作为参数传递给构造函数
 * 
 */
void make_from_tuple_test(){
    auto t = std::make_tuple(1, 2, 3);
    class A{
    public:
        A(int a, int b, int c){
            cout << a << " " << b << " " << c << endl;
        }
    };
    A a = std::make_from_tuple<A>(t);
}

/**
 * @brief from_chars
 * 
 */
void charconv_test(){
    const string str("1234567890");
    int value = 0;
    const auto res = std::from_chars(str.data(), str.data() + 4, value, 10);
    if (res.ec == std::errc()) {
        cout << value <<", distance " << (res.ptr-str.data()) << endl;
    }
    else if (res.ec == std::errc::result_out_of_range) {
        cout << "out of range" << endl;
    }
    else {
        cout << "parse error" << endl;
    }

    /// 最后一个参数base=8，将字符串视为八进制
    const auto res1 = std::from_chars(str.data(), str.data() + 4, value, 8);
    if (res1.ec == std::errc()) {
        cout << value <<", distance " << (res1.ptr-str.data()) << endl;
    }
    else if (res1.ec == std::errc::result_out_of_range) {
        cout << "out of range" << endl;
    }
    else {
        cout << "parse error" << endl;
    }

    auto str2 = std::string("12.34");
    // double value2 = 0;
    // const auto format = std::chars_format::general;
    ///@bug 这里不支持std::chars_format::general???
    // const auto res2 = std::from_chars(str2.data(), str2.data() + str2.size(), value2, std::chars_format::general);
    int value2 = 0;
    const auto res2 = std::from_chars(str2.data(), str2.data() + str2.size(), value2); 
 
    if (res2.ec == std::errc()) {
        cout << value2 <<", distance " << (res2.ptr-str2.data()) << endl; /// 输出12, distance 2
    }
    else if (res2.ec == std::errc::result_out_of_range) {
        cout << "out of range" << endl;
    }
    else {
        cout << "parse error" << endl;
    }
}

/**
 * @brief 不懂这里的as_const的作用是什么，传参时方便(auto, T, etc)自动推断？
 * 
 */
void as_const_test(){
    string str = "hello";
    auto &&cstr = std::as_const(str);
    const auto &cstr2 = str; 
    cout << cstr << endl;
    cout << cstr2 << endl;
}

int main(){
    cout << "variant test: \n";
    variant_test();
    cout << "optional test: \n";
    optional_test();
    cout << "any test: \n";
    any_test();
    cout << "apply test: \n";
    apply_test();
    cout << "make_from_tuple test: \n";
    make_from_tuple_test();
    /// file_system
    /// string_view
    cout << "charconv test: \n";
    charconv_test();
    cout << "as_const test: \n";
    as_const_test();
}